package org.zella.smartrat;

import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zella.smartrat.config.impl.TypesafeConfig;
import org.zella.smartrat.core.Core;
import org.zella.smartrat.net.Server;
import org.zella.smartrat.net.SocketIO;
import org.zella.smartrat.torrent.webtorrent.impl.DefaultWebTorrent;

import java.util.List;

public class RunnerMain {

    private static final Logger logger = LoggerFactory.getLogger(RunnerMain.class);

    public static void main(String[] args) {
        var config = new TypesafeConfig(ConfigFactory.defaultApplication().resolve());

        var torrent = new DefaultWebTorrent(config.webTorrentExecutable());
        var core = new Core(torrent, config);

        //TODO search files
        SocketIO.createWithReconnection(config.ratSearchUrl(), List.of("remoteSearchFiles", "remoteSearchTorrent"))
                .flatMap(rxSocket -> new Server(rxSocket, core, config).create())
                .subscribe(httpServer -> logger.info("Server started at port: " + httpServer.actualPort()));


    }
}
