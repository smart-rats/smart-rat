package org.zella.smartrat.config;

public interface IConfig {
    int port();

    String ratSearchUrl();

    String webTorrentExecutable();
}
