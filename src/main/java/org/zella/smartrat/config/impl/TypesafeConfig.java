package org.zella.smartrat.config.impl;

import com.typesafe.config.Config;
import org.zella.smartrat.config.IConfig;

public class TypesafeConfig implements IConfig {

    private final Config config;

    public TypesafeConfig(Config config) {
        this.config = config;
    }

    @Override
    public int port() {
        return config.getInt("http.port");
    }

    @Override
    public String ratSearchUrl() {
        return config.getString("ratSearch.url");
    }

    @Override
    public String webTorrentExecutable() {
        return config.getString("webTorrent.executable");
    }

}
