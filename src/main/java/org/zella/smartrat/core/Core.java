package org.zella.smartrat.core;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zella.smartrat.config.IConfig;
import org.zella.smartrat.media.IMediaType;

import org.zella.smartrat.model.SearchItem;
import org.zella.smartrat.net.model.SearchInput;
import org.zella.smartrat.net.model.TorrentMeta;
import org.zella.smartrat.torrent.filter.FilteredItem;
import org.zella.smartrat.torrent.filter.ITorrentFilter;
import org.zella.smartrat.torrent.webtorrent.IWebTorrent;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Core {

    private static final Logger logger = LoggerFactory.getLogger(Core.class);

    private final IWebTorrent torrent;

    private final int processors = Runtime.getRuntime().availableProcessors();
    //2 minimum TODO conf
    private final int torrentFileParallelism = processors < 2 ? 2 : processors;

    private final int ratSearchPeers = 3;

    private AtomicReference<Disposable> currentPlay = new AtomicReference<>();

    public Core(IWebTorrent torrent, IConfig conf) {
        this.torrent = torrent;
    }

    //TODO search files rat search support
    public Single<List<FilteredItem>> search(Observable<String> searchOut,
                                             IMediaType cmd,
                                             int collectTimeSummary,
                                             int pageSize,
                                             List<SearchInput.SearchExcept> searchExcept) {
        var collectTimeFetchFiles = collectTimeSummary / 2;
        var collectTimeBuffer = collectTimeSummary - collectTimeFetchFiles;
        var timeout = Math.round(collectTimeSummary * 1.5f);
        var notSearch = new HashMap<String, Set<Integer>>();
        searchExcept.forEach(e -> notSearch.merge(e.hash, e.indexes, (idx, idx2) -> Stream.concat(idx.stream(), idx2.stream())
                 .collect(Collectors.toSet())));

        return searchOut
                .map(SearchItem::fromString)
                .map(items -> io.vavr.collection.List.ofAll(items)
                        .filter(i -> i.seeders() >= cmd.minSeeders())
                        .distinctBy(SearchItem::hash)
                        .sortBy(SearchItem::seeders).reverse().asJava())
                .filter(items -> !items.isEmpty())
                //items - single ratsearch search
                .flatMap(items -> {
                    logger.debug("Selected rat search items: " + items.size());
                    return Observable.merge(items.stream().map(searchItem ->
                                    torrent.fetchFiles(searchItem.hash())
                                            .map(what -> ITorrentFilter.flattenByScores(cmd.filter(TorrentMeta.fromSearchItem(searchItem), pageSize)
                                                    //TODO single torrent timeout, should be on caller side
                                                    .stream().map(f -> f.selectFiles(what)).collect(Collectors.toList()))).toObservable()
                                            .onExceptionResumeNext(e -> Observable.empty())).collect(Collectors.toList()),
                            //TODO conf
                            torrentFileParallelism)
                            //single lucene search
                            .filter(search -> !search.isEmpty())
                            //skip search except
                            .map(search -> search.entrySet().stream().filter(kv -> {
                                var hash = kv.getValue().file.hash;
                                var index = kv.getValue().file.index;
                                //TODO test
                           //     return true;
                                return (!notSearch.containsKey(hash)) || (!notSearch.get(hash).contains(index));
                            }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)))
                            //if no TIMED! buffer then result will be fasted N from. With buffer we wait others and sort flattened search result
                            .buffer(collectTimeFetchFiles, TimeUnit.SECONDS, items.size())
                            .filter(search -> !search.isEmpty())
                            .map(collectedSearches -> {
                                logger.debug("Selected lucene search items " + collectedSearches.size());
                                var files = new ArrayList<FilteredItem>();
                                collectedSearches.forEach(map -> files.addAll(map.values()));
                                return io.vavr.collection.List.ofAll(files).sortBy(i -> i.score).reverse().asJava();
                            }); // - here no timeouts
                }, torrentFileParallelism)
                //TODO not tested
                .buffer(collectTimeBuffer, TimeUnit.SECONDS, ratSearchPeers)
                .filter(superSearches -> !superSearches.isEmpty())
                .map(superSearches -> io.vavr.collection.List.ofAll(superSearches).flatMap(Function.identity()).sortBy(i -> i.score).reverse().asJava())
                .filter(search -> !search.isEmpty())
                .firstOrError()
                .map(result -> result.stream().limit(pageSize).collect(Collectors.toList()))
                .timeout(timeout, TimeUnit.SECONDS)
                .onErrorReturnItem(List.of());
    }

    public Single<FilteredItem> searchAndPlayNow(Observable<String> searchOut, IMediaType cmd, String streaming, int collectTimeSec) {
        //TODO bad computation chain but ...
        return Single.create(emitter -> {
            if (currentPlay.get() != null)
                currentPlay.get().dispose();
            currentPlay.set(search(searchOut, cmd, collectTimeSec, 1, List.of())//TODO in case of album - another code
                    //TODO now only one file supported
                    .map(whatToPlay -> whatToPlay.stream().findFirst().get())
                    .flatMapObservable(playable -> {
                        emitter.onSuccess(playable);
                        logger.debug("Start playing " + playable.file.toString());
                        //TODO if torrent slow, try next =)
                        return torrent.streamFile(playable.file.hash, playable.index, streaming);
                    }).subscribe(dontCare -> {
                    }, emitter::onError));
        });
    }

    public Single<Boolean> playNow(String hash, int index, String streaming) {
        return Single.fromCallable(() -> {
            if (currentPlay.get() != null)
                currentPlay.get().dispose();
            currentPlay.set(torrent.streamFile(hash, index, streaming).subscribe());
            return true;
        });
    }

}
