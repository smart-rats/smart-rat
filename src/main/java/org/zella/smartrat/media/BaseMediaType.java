package org.zella.smartrat.media;

public abstract class BaseMediaType implements IMediaType {

    protected final String subject;

    protected BaseMediaType(String subject) {
        this.subject = subject;
    }

    @Override
    public String subject() {
        return subject;
    }

    /**
     * @param mb megabytes
     * @return bytes
     */
    protected long mbToBytes(long mb) {
        return mb * 1024 * 1024;
    }

    protected long kbToBytes(long kb) {
        return kb * 1024;
    }
}
