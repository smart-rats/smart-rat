package org.zella.smartrat.media;

import io.vavr.control.Option;
import org.zella.smartrat.net.model.TorrentMeta;
import org.zella.smartrat.torrent.filter.FilteredItem;
import org.zella.smartrat.torrent.filter.ITorrentFilter;

import java.util.List;

public interface IMediaType {

    String subject();

    int minSeeders();

    Option<Long> recommendedSize();

    Option<Long> maxSize();

    Option<List<String>> extensions();

    //TODO not beautifull
    List<ITorrentFilter> filter(TorrentMeta torrentMeta, int limit);

}
