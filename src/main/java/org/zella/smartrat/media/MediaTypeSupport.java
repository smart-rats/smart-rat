package org.zella.smartrat.media;

import io.reactivex.Single;
import org.zella.smartrat.media.impl.AnyMediaType;
import org.zella.smartrat.media.impl.FilmMediaType;
import org.zella.smartrat.media.impl.SongMediaType;

public class MediaTypeSupport {
    public static Single<IMediaType> extract(String mediaType, String subject) {
        return Single.fromCallable(() -> {
            if (SongMediaType.Word.equals(mediaType)) {
                return new SongMediaType(subject);
            } else if (FilmMediaType.Word.equals(mediaType)) {
                return new FilmMediaType(subject);
            } else return new AnyMediaType(subject);
        });
    }

}
