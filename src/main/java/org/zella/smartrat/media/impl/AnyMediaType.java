package org.zella.smartrat.media.impl;

import io.vavr.control.Option;
import org.zella.smartrat.media.BaseMediaType;
import org.zella.smartrat.net.model.TorrentMeta;
import org.zella.smartrat.torrent.filter.ITorrentFilter;
import org.zella.smartrat.torrent.filter.impl.FilesOnlyLuceneFilter;

import java.util.List;

public class AnyMediaType extends BaseMediaType {

    public AnyMediaType(String subject) {
        super(subject);
    }

    @Override
    public int minSeeders() {
        return 1;
    }

    @Override
    public Option<Long> recommendedSize() {
        return Option.none();
    }

    @Override
    public Option<Long> maxSize() {
        return Option.none();
    }

    @Override
    public Option<List<String>> extensions() {
        return Option.none();
    }

    @Override
    public List<ITorrentFilter> filter(TorrentMeta torrentMeta, int limit) {
        return List.of(new FilesOnlyLuceneFilter(subject, extensions(), limit, torrentMeta));
    }
}