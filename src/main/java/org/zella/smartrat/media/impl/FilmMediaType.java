package org.zella.smartrat.media.impl;

import io.vavr.control.Option;
import org.zella.smartrat.media.BaseMediaType;
import org.zella.smartrat.net.model.TorrentMeta;
import org.zella.smartrat.torrent.filter.ITorrentFilter;
import org.zella.smartrat.torrent.filter.impl.FilesOnlyLuceneFilter;

import java.util.List;

public class FilmMediaType extends BaseMediaType {

    public FilmMediaType(String subject) {
        super(subject);
    }

    @Override
    public int minSeeders() {
        return 3;
    } //TODO conf and search have different values

    @Override
    public Option<Long> recommendedSize() {
        return Option.of(mbToBytes(7000));
    }

    @Override
    public Option<Long> maxSize() {
        return Option.of(mbToBytes(50000));
    }

    @Override
    public Option<List<String>> extensions() {
        return Option.of(List.of("mkv", "avi", "mp4", "mov", "wmv", "flv"));
    }

    @Override
    public List<ITorrentFilter> filter(TorrentMeta torrentMeta, int limit) {
        return List.of(new FilesOnlyLuceneFilter(subject, extensions(), limit, torrentMeta));
    }

    public static final String Word = "film";
}