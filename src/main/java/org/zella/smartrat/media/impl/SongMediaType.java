package org.zella.smartrat.media.impl;

import io.vavr.control.Option;
import org.zella.smartrat.media.BaseMediaType;
import org.zella.smartrat.net.model.TorrentMeta;
import org.zella.smartrat.torrent.filter.FilteredItem;
import org.zella.smartrat.torrent.filter.ITorrentFilter;
import org.zella.smartrat.torrent.filter.impl.FilesOnlyLuceneFilter;

import java.util.List;

public class SongMediaType extends BaseMediaType {

    public SongMediaType(String subject) {
        super(subject);
    }

    @Override
    public int minSeeders() {
        return 1;
    }

    @Override
    public Option<Long> recommendedSize() {
        return Option.of(mbToBytes(20));
    }

    @Override
    public Option<Long> maxSize() {
        return Option.of(mbToBytes(1000));
    }

    @Override
    public Option<List<String>> extensions() {
        return Option.of(List.of("mp3", "flac", "m4a", "wav", "aac"));
    }

    @Override
    public List<ITorrentFilter> filter(TorrentMeta torrentMeta, int limit) {
        return List.of(new FilesOnlyLuceneFilter(subject, extensions(), limit, torrentMeta));
    }

    public static final String Word = "song";
}