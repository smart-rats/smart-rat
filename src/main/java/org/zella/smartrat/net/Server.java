package org.zella.smartrat.net;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.core.http.HttpServer;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.BodyHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zella.smartrat.config.IConfig;
import org.zella.smartrat.media.MediaTypeSupport;
import org.zella.smartrat.media.IMediaType;
import org.zella.smartrat.core.Core;
import org.zella.smartrat.net.model.PlayInput;
import org.zella.smartrat.net.model.SearchAndPlayInput;
import org.zella.smartrat.net.model.SearchInput;
import org.zella.smartrat.net.model.SearchOutput;


import java.util.List;

public class Server {

    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    private final SocketIO.RxSocket socket;

    private final Core core;

    private final IConfig config;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private static final String SearchParam;

    //TODO jackson
    static {
        JSONObject j = new JSONObject();
        try {
            j.put("limit", 50);
            j.put("safeSearch", true); //TODO conf
            j.put("orderBy", "seeders");
            j.put("orderDesc", true);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        SearchParam = j.toString();
    }

    public Server(SocketIO.RxSocket socket, Core core, IConfig config) {

        this.socket = socket;
        this.core = core;
        this.config = config;
    }

    public Single<HttpServer> create() {

        Router router = Router.router(Vertx.vertx());

        router.get("/healthcheck").handler(ctx -> ctx.response().end("ok"));
        router.post().handler(BodyHandler.create());
        router.post("/api/v1/search").handler(ctx -> {
            logger.debug("Search...");
            readBody(ctx, SearchInput.class).flatMap(in -> (MediaTypeSupport.extract(in.mediaType, in.text))
                    .flatMap(mediaType ->
                            core.search(
                                    searchRequest(mediaType).andThen(socket.out),
                                    mediaType,
                                    in.maxSearchTimeSec,
                                    in.pageSize,
                                    in.searchExcept)))
                    .subscribe(items -> {
                                logger.debug("Send search response");
                                ctx.response().end(
                                        objectMapper.writeValueAsString(new SearchOutput(items)));
                            },
                            err -> {
                                logger.error("Error", err);
                                ctx.fail(err);
                            });

        });
        router.post("/api/v1/searchAndPlay").handler(ctx -> {
            logger.debug("Search and play...");
            readBody(ctx, SearchAndPlayInput.class).flatMap(in -> (MediaTypeSupport.extract(in.mediaType, in.text))
                    .flatMap(mediaType ->
                            core.searchAndPlayNow(
                                    searchRequest(mediaType).andThen(socket.out),
                                    mediaType,
                                    in.streaming,
                                    in.maxSearchTimeSec)))
                    .subscribe(item -> ctx.response().end(
                            objectMapper.writeValueAsString(item.file.toHumanText())),
                            err -> {
                                logger.error("Error", err);
                                ctx.fail(err);
                            });
        });

        router.post("/api/v1/play").handler(ctx -> {
            logger.debug("Play...");
            readBody(ctx, PlayInput.class).flatMap(in ->
                    core.playNow(in.hash, in.index, in.streaming))
                    .subscribe(b -> ctx.response().end(), err -> {
                        logger.error("Error", err);
                        ctx.fail(err);
                    });
        });

        return Vertx.vertx().createHttpServer().
                requestHandler(router).rxListen(config.port());
    }

    private Completable searchRequest(IMediaType type) {
        return Completable.fromRunnable(() -> {
            socket.in.onNext(new SocketIO.SocketInput("searchTorrent", List.of(type.subject(), SearchParam)));
            socket.in.onNext(new SocketIO.SocketInput("searchFiles", List.of(type.subject(), SearchParam)));
        });
    }

    private <T> Single<T> readBody(RoutingContext body, Class<T> valueType) {
        return Single.fromCallable(() -> {
            var d = objectMapper.readValue(body.getBodyAsString(), valueType);
            return d;
        });
    }
}
