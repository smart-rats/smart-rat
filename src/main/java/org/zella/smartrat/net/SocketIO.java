package org.zella.smartrat.net;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class SocketIO {

    private static final Logger logger = LoggerFactory.getLogger(SocketIO.class);

    //can be disconnected by close/err event to input stream
    public static Single<RxSocket> create(String url, List<String> events, IO.Options opts) {

        return Single.create(emitter -> {

            ReplaySubject<SocketInput> in = ReplaySubject.createWithSize(16);
            //TODO it's ok?;
            Subject<String> out = PublishSubject.create();

            Socket socket = IO.socket(url, opts);

            socket
                    .on(Socket.EVENT_CONNECT, args -> logger.debug("connected " + url))
                    .on(Socket.EVENT_CONNECT_ERROR, args -> logger.warn("connect_error " + url))
                    .on(Socket.EVENT_CONNECT_TIMEOUT, args -> logger.warn("connect_timeout " + url))
                    .on(Socket.EVENT_DISCONNECT, args -> logger.info("disconnect " + url));

            events.forEach(e -> socket.on(e, args -> {
                logger.debug("From socket: " + Arrays.toString(args));
                if (args[0] != null)
                    out.onNext((args[0].toString()));
            }));
            logger.debug("Try to connecting to " + url);
            if (!socket.connected())
                socket.connect();
            in.subscribe(
                    input -> {
                        logger.debug("send event " + input.event);
                        socket.emit(input.event, input.args.toArray(), ack -> {
                        });
                    }, err -> socket.disconnect(),
                    socket::disconnect);
            emitter.onSuccess(new RxSocket(in, out));
        });
    }


    public static Single<RxSocket> createWithReconnection(String url, List<String> events) {
        IO.Options opts = new IO.Options();
        opts.reconnection = true;
        return create(url, events, opts);
    }

    public static class RxSocket {
        public final Observer<SocketInput> in;
        public final Observable<String> out;

        RxSocket(Observer<SocketInput> in, Observable<String> out) {
            this.in = in;
            this.out = out;
        }
    }

    public static class SocketInput {
        public final String event;
        public final List<Object> args;

        public SocketInput(String event, List<Object> args) {
            this.event = event;
            this.args = args;
        }
    }
}
