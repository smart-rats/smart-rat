package org.zella.smartrat.net.model;

import org.zella.smartrat.torrent.webtorrent.TorrentFileDesc;

//TODO rem it
public class FoundItem {

    public int searchNum;
    public TorrentFileDesc file;

    public FoundItem() {
    }

    public FoundItem(int searchNum, TorrentFileDesc file) {
        this.searchNum = searchNum;
        this.file = file;
    }
}
