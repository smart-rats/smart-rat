package org.zella.smartrat.net.model;

public class SearchAndPlayInput {

    public String mediaType;
    public String text;
    public String streaming;
    public int maxSearchTimeSec;

    public SearchAndPlayInput() {
    }

    public SearchAndPlayInput(String mediaType, String text, String streaming, int maxSearchTimeSec, int timeoutSec) {
        this.mediaType = mediaType;
        this.text = text;
        this.streaming = streaming;
        this.maxSearchTimeSec = maxSearchTimeSec;
    }
}
