package org.zella.smartrat.net.model;

import java.util.List;
import java.util.Set;

public class SearchInput {

    public String mediaType;
    public String text;
    public int maxSearchTimeSec;
    public int pageSize;
    public List<SearchExcept> searchExcept;

    public SearchInput() {
    }


    public static class SearchExcept {

        public String hash;
        public Set<Integer> indexes;

        public SearchExcept() {
        }
    }
}
