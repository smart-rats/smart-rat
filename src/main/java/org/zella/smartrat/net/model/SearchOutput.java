package org.zella.smartrat.net.model;

import org.zella.smartrat.torrent.filter.FilteredItem;

import java.util.List;

public class SearchOutput {

    public List<FilteredItem> items;

    public SearchOutput() {
    }

    public SearchOutput(List<FilteredItem> items) {
        this.items = items;
    }


}
