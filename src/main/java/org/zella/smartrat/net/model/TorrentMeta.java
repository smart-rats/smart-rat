package org.zella.smartrat.net.model;

import org.zella.smartrat.model.SearchItem;

public class TorrentMeta {
    public final int seeders;

    public TorrentMeta(int seeders) {
        this.seeders = seeders;
    }

    public static TorrentMeta fromSearchItem(SearchItem item) {
        return new TorrentMeta(item.seeders());
    }

}