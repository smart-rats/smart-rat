package org.zella.smartrat.torrent.filter;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.ByteBuffersDirectory;
import org.apache.lucene.store.Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zella.smartrat.net.model.TorrentMeta;
import org.zella.smartrat.torrent.webtorrent.TorrentFileDesc;
import org.zella.smartrat.torrent.webtorrent.impl.DefaultWebTorrent;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseLuceneFilter implements ITorrentFilter {

    private static final Logger logger = LoggerFactory.getLogger(DefaultWebTorrent.class);

    protected abstract Document searchableDocument(TorrentFileDesc file);

    protected abstract Query query(Analyzer indexAnalyzer);

    protected abstract int count();

    protected abstract Analyzer analyzer();

    protected abstract TorrentMeta torrentMeta();

    @Override
    public Map<Integer, FilteredItem> selectFiles(Map<Integer, TorrentFileDesc> structure) {
        logger.debug("Start filtering");
        Directory memoryDir = null;
        try {
            memoryDir = new ByteBuffersDirectory();

            Analyzer analyzer = analyzer();
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
            IndexWriter writter = new IndexWriter(memoryDir, indexWriterConfig);
            structure.forEach((i, file) -> {
                Document document = searchableDocument(file);
                document.add(new StringField("i", i.toString(), Field.Store.YES));
                try {
                    writter.addDocument(document);
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            });

            writter.commit();
            writter.close();

            IndexReader reader = DirectoryReader.open(memoryDir);
            IndexSearcher searcher = new IndexSearcher(reader);

            TopDocs docs = searcher.search(query(analyzer), count());
            ScoreDoc[] hits = docs.scoreDocs;

            logger.debug("Found " + hits.length);

            Map<Integer, FilteredItem> out = new HashMap<>();
            for (ScoreDoc hit : hits) {
                var i = Integer.parseInt(searcher.doc(hit.doc).get("i"));
                out.put(i, new FilteredItem(structure.get(i), hit.score, i, torrentMeta()));
            }
            logger.debug("End filtering");
            return out;


        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } finally {
            try {
                memoryDir.close();
            } catch (IOException e) {
                logger.error("Can't close memory index", e);
            }
        }

    }
}
