package org.zella.smartrat.torrent.filter;

import org.zella.smartrat.net.model.TorrentMeta;
import org.zella.smartrat.torrent.webtorrent.TorrentFileDesc;

public class FilteredItem {

    public final TorrentFileDesc file;
    public final float score;
    public final int index;

    public final TorrentMeta torrentMeta;

    public FilteredItem(TorrentFileDesc file, float score, int index, TorrentMeta torrentMeta) {
        this.file = file;
        this.score = score;
        this.index = index;
        this.torrentMeta = torrentMeta;
    }

    @Override
    public String toString() {
        return "FilteredItem{" +
                "file=" + file +
                ", score=" + score +
                '}';
    }

}
