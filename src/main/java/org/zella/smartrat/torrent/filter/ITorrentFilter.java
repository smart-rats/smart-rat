package org.zella.smartrat.torrent.filter;

import org.zella.smartrat.torrent.webtorrent.TorrentFileDesc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ITorrentFilter {

    Map<Integer, FilteredItem> selectFiles(Map<Integer, TorrentFileDesc> torrentFileStructure);

    /**
     * Users in multi filter Seq[IFilter] => flatten remove duplicates
     *
     * @param r
     * @return
     */
    static Map<Integer, FilteredItem> flattenByScores(List<Map<Integer, FilteredItem>> r) {
        var m = new HashMap<Integer, FilteredItem>();
        r.forEach(map -> map.forEach((i, v) -> {
            if (!m.containsKey(i) || m.get(i).score < v.score)
                m.put(i, v);
        }));
        return m;

    }

}
