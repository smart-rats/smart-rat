package org.zella.smartrat.torrent.filter;

import org.zella.smartrat.net.model.TorrentMeta;

import java.util.Map;
import java.util.stream.Collectors;

public class TorrentFilters {
    public static ITorrentFilter downloadAll(TorrentMeta torrentMeta) {
        return torrentFileStructure -> torrentFileStructure.entrySet().stream()
                .map(kv -> Map.entry(kv.getKey(), new FilteredItem(kv.getValue(), -1, kv.getKey(), torrentMeta)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
