package org.zella.smartrat.torrent.webtorrent;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

import java.nio.file.Path;
import java.util.Map;

public interface IWebTorrent {
    Completable downloadFile(String hash, Path dir, int index);

    Completable download(String hash, Path dir);

    Single<Map<Integer, TorrentFileDesc>> fetchFiles(String hash);

    Observable<TorrentState> streamFile(String hash, int index, String stream);
}
