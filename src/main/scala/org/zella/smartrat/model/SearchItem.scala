package org.zella.smartrat.model

import play.api.libs.json.{JsValue, Json, OFormat}

import scala.collection.JavaConverters._

case class SearchItem(leechers: Int,
                      //                       bad: Double,
                      //                       added: Double,
                      //                       completed: Double,
                      //                       good: Double,
                      //   contentCategory: String,
                      //  piecelength: Double,
                      seeders: Int,
                      //                       size: Double,
                      //                       peer: Peer,
                      name: String,
                      //                       files: Double,
                      //                       trackersChecked: Double,
                      //                       contentType: String,
                      hash: String,
                      // info: Info
                     )

object SearchItem {
  implicit val format: OFormat[SearchItem] = Json.format[SearchItem]

  def fromString(s: String): java.util.List[SearchItem] = {
    Json.parse(s).as[Seq[SearchItem]].asJava
  }

  def toJsonString(items: java.util.List[SearchItem]): String = {
    Json.toJson(items.asScala).toString()
  }

  def flatten(lists: java.util.List[java.util.List[SearchItem]]): java.util.List[SearchItem] = {
    lists.asScala.flatMap(_.asScala).asJava
  }
}

case class Peer(address: String, port: Double)

object Peer {
  implicit val format: OFormat[Peer] = Json.format[Peer]
}

case class Info(trackers: List[String],
                name: String,
                description: String,
                rutrackerThreadId: Double,
                poster: String,
                contentCategory: String)

object Info {
  implicit val format: OFormat[Info] = Json.format[Info]
}

