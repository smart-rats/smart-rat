package org.zella.smartrat.model

import play.api.libs.json.{Json, OFormat}

//TODO remove all scala code
case class SearchParams(limit: Int = 100, safeSearch: Boolean = true, orderBy: Option[String] = None, orderDesc: Boolean = false)

object SearchParams {
  implicit val format: OFormat[SearchParams] = Json.format[SearchParams]
}
