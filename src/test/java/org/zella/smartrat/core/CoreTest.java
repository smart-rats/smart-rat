package org.zella.smartrat.core;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;
import io.vavr.control.Option;
import org.junit.Ignore;
import org.junit.Test;
import org.zella.smartrat.config.IConfig;
import org.zella.smartrat.media.IMediaType;
import org.zella.smartrat.model.SearchItem;
import org.zella.smartrat.net.SocketIO;
import org.zella.smartrat.net.model.TorrentMeta;
import org.zella.smartrat.torrent.filter.TorrentFilters;
import org.zella.smartrat.torrent.webtorrent.IWebTorrent;
import org.zella.smartrat.torrent.webtorrent.TorrentFileDesc;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;

public class CoreTest {

    //TODO simulate fetch files time, for now test - bad
    @Test
    public void shouldCollectIn5Second() throws InterruptedException {

        var torrent = mock(IWebTorrent.class);
        when(torrent.fetchFiles("hash1")).thenReturn(Single.just(
                Map.of(1, new TorrentFileDesc(1, new String[]{"file1"}, 16, "hash1"))
        ));
        when(torrent.streamFile(eq("hash1"), anyInt(), anyString())).thenReturn(Observable.empty());

        var config = mock(IConfig.class);
//        when(config.searchPageSize()).thenReturn(10);

        var item1 = SearchItem.toJsonString(List.of(SearchItem.apply(-1, 9, "name1", "hash1")));
        var item2 = SearchItem.toJsonString(List.of(SearchItem.apply(-1, 8, "name2", "hash2")));

        var searchOut = Observable.interval(2, TimeUnit.SECONDS).map
                (i -> {
                    if (i == 0) return item1;
                    else return item2;
                }).take(10);

        var mediaCommand = mock(IMediaType.class);
        when(mediaCommand.extensions()).thenReturn(Option.none());
        when(mediaCommand.maxSize()).thenReturn(Option.none());
        when(mediaCommand.recommendedSize()).thenReturn(Option.none());
        when(mediaCommand.subject()).thenReturn("text1");
        when(mediaCommand.minSeeders()).thenReturn(9);
        //9 same as seeders
        when(mediaCommand.filter(any(), anyInt())).thenReturn(List.of(TorrentFilters.downloadAll(new TorrentMeta(-1))));

        var stream = new Core(torrent, config)
                .searchAndPlayNow(searchOut, mediaCommand, "streaming_test", 5)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io());

        var testObs = TestObserver.create();

        stream.subscribe(testObs);

        Thread.sleep(2500);

        testObs.assertNoErrors();
        testObs.assertNotComplete();

        Thread.sleep(600);
        testObs.assertNoErrors();
        testObs.assertComplete();

    }

}
