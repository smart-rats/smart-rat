package org.zella.smartrat.torrent

import org.mockito.Mockito
import org.scalatest._
import org.zella.smartrat.config.IConfig
import org.zella.smartrat.net.model.TorrentMeta
import org.zella.smartrat.torrent.filter.impl.FilesOnlyLuceneFilter
import org.zella.smartrat.torrent.webtorrent.impl.DefaultWebTorrent

import scala.collection.JavaConverters._

class LuceneFileFilterSpec extends FlatSpec with Matchers {

  "LuceneFileFilter" should "find" in {

    val src = new FilesOnlyLuceneFilter("бутусов крылья", io.vavr.control.Option.of(List("mp3").asJava), 10, new TorrentMeta(-1))

    val tor = new DefaultWebTorrent(sys.env("WEB_TORRENT_EXECUTABLE")).fetchFiles("cb9d759d373a6c2575577561f544b8e17e0b9798").blockingGet()

    val result = src.selectFiles(tor)

    result.asScala.head._2.file.paths.last.contains("Крылья.mp3") shouldBe true
    println(result)

  }


}
